from django.contrib import auth
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render


# Create your views here.

def index1(requst_arg):
    return render(requst_arg, 'index.html')


def case_mange_fuc(request_arg):
    if request_arg.method == 'POST':
        httpret = text_change(request_arg)
        return httpret
    else:
        return render(request_arg, 'casemanage.html', {'case': 'management'})


def text_change(request_arg):
    if request_arg.method == 'POST':
        text1 = request_arg.POST.get('text_id')
        print(text1)
        if len(text1) == 0:
            textChan = '默认填充'
        else:
            textChan = text1
    else:
        textChan = '默认填充'
    return render(request_arg, 'casemanage.html', {'case': textChan, 'text': text1})
