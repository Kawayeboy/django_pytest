# -*- coding: utf-8 -*-
'''
pytest 框架规范
    1、py 测试文件必须 test_ 开头（或者以 _test 结尾）
    2、测试类 必须以 Test 开头,注意Test 的 T 为大写,并且不能用init 方法
    3、测试方法必须以 test_ 开头
    4、断言必须使用 assert 方法
    5、在执行pytest命令时，会自动从当前目录及子目录中寻找符合上述约束的测试函数来执行。
    6、命令行中执行测试：pytest 文件路径/测试文件名。例如：pytest d:test/test.py
'''
import pprint

import pytest
from UIappliction.caseClassLibs.BaseTestClass import Login, Api
from UIappliction.caseDataAnalysisTools.dataAnalysis import AnalysisFiles
from UIappliction.configs.config import HOST


def get_param():
    param_list = AnalysisFiles().analysis_files_data()
    return param_list


class TestLogin:

    @pytest.mark.parametrize('param, resp, detail, url, method', get_param())
    def test_one(self, param, resp, detail, url, method):
        if url == '/ToPublic_API/login/userlogin':
            response = Login().login(param, url, method)
            assert resp["type"] == response["type"]
            assert resp["msg"] == response['msg']
        else:
            param_login = {'account': '900000194910010000', 'password': 'abc', 'channel': 'pc'}
            web_url = f'{HOST}' + '/ToPublic_API/login/userlogin'
            method_login = 'POST'
            token_str = Login().login(param_login, web_url, method_login, model=False)
            header = {'token': token_str}
            Api().api_case(param, url, method, header)


if __name__ == "__main__":
    pytest.main(["-s"])    # 通过主函数的方式执行：调用 pytest 的main方法来执行所有的测试用例

