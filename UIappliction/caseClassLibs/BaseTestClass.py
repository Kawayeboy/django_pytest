# -*- coding: utf-8 -*-
from pprint import pprint
import pytest

from UIappliction.configs.config import HOST, Secret_Key
import requests
import hashlib
from Crypto.Cipher import AES
import base64
#from cryptography.hazmat.primitives import padding
#from cryptography.hazmat.primitives.ciphers import algorithms

print()
'''
接口名称：登陆接口
用途：
    1-登陆接口的自动化测试
    2-获取登陆接口的token，给后续接口使用
'''

'''
封装一个加密方法，普通的md5加密，与例的接口加密不同，后续会写一个加密本例的加密方法
'''
def get_md5(password= 'abc'):
    # 实例化MD5对象
    md5 = hashlib.md5()
    # 调用加密方法
    md5.update(password.encode("utf-8"))
    # 返回加密结果
    return md5.hexdigest()

'''
封装一个 AES 秘钥的处理方法，一般密码为16位或者16位的倍数。
该方法的就是，当秘钥不足16位时，要补足16位字节数据，注意是字节数据，所以需要先将字符串进行 encode(‘utf-8’)处理
字节补位的方式有多种，例如：空格补位、\x00补位、通过对不足的位数的 ASCII 码的utf-8 解释符进行补位（本例用的就是这种）
'''
def add_secret_key(password):
    password_encode = password.encode("utf-8")
    if len(password_encode) % 16:
        # print(len(password_encode))
        add_text = 16 - (len(password_encode) % 16)
        # print(len(password_encode) % 16)
        # print(16 % 16)
        # print(add_text)
    else:
        add_text = 0
    text_password = password_encode + (chr(add_text).encode("utf-8") * add_text)  # chr 获取ASCII 码
    # print("chr--------------------------------------------")
    # print(chr(add_text))
    # print(chr(1))
    # print(chr(1).encode('utf-8'))
    # print(chr(add_text).encode("utf-8"))
    # print("chr--------------------------------------------")
    # print(text_password)
    return text_password


'''
封装一个AES 加密的方法。AES加密方式有五种：ECB, CBC, CTR, CFB, OFB。这里使用ECB方式加密
AES 加密解密需要安装 pycryptodome 这个库。from Crypto.Cipher import AES 导入对应的库
AES 加密需要几个参数：秘钥、模型、加密明文内容。由于python3的字符串是Unicode编码，所以需要encode转化成字节类型数据
'''
def encryption(password):
    password_encode = add_secret_key(password)   # 明文内容类型转化
    model = AES.MODE_ECB     # 定义AES 加密方式，也就是模型
    # print(f'{Secret_Key}')
    key = f'{Secret_Key}'  # 获取加密的秘钥，同时对秘钥进行处理
    key = key.encode('utf-8')
    aes = AES.new(key, model)   # 通过秘钥和模型创建一个aes对象
    encry_text = aes.encrypt(password_encode)  # 通过aes 对象调用encrypt方法对明文进行加密
    # print(encry_text)
    encry_text = base64.encodebytes(encry_text)  # 将返回的字节型数据转进行base64编码
    # print(encry_text)
    en_text = encry_text.decode('utf8')  # 将字节型数据转换成python中的字符串类型
    return en_text.strip()   # Python strip() 方法用于移除字符串头尾指定的字符（默认为空格或换行符）或字符序列。


'''
@staticmethod
def pkcs7_padding(data):
    if not isinstance(data, bytes):
        data = data.encode()
    padder = padding.PKCS7(algorithms.AES.block_size).padder()
    padded_data = padder.update(data) + padder.finalize()
    return padded_data

'''

'''
封装一个AES解密方法
'''
def decrypt(test):
    pass

# 封装一个登陆的类
class Login:
    def login(self, argData, url, method, model=True):
        # url 配置 /ToPublic_API/login/userlogin
        webUrl = f'{HOST}' + url # {HOST} 写法为普通的字符参数的调用写法，f的意思是格式化
        # 接口参数配置,传入的参数为字典类型，但是密码需要解密后请求才有效，所以可以对字典进行重新赋值
        payload = argData
        # 请求获取响应对象，需要导入requests 库
        '''
        请求的参数格式一般有，根据实际传参格式适用对应的写法，例如本接口未data格式
            data -- 一般表单格式
            json -- json格式数据
            files -- 文件类型，适用文件上传接口
            params -- 参数会放到url后面，? 1=1&2=2
        '''
        if method == 'POST':
            resp = requests.post(webUrl, payload)
        else:
            resp = requests.get(webUrl, payload)
        print(resp.text)  # json 格式
        print(resp.json())  # 字典格式，即通过json() 方法将返回内容进行格式转化
        # 查看相应
        if model == True:
            return resp.json()
        else:
            resp = resp.json()
            return resp['token']


class Api:
    def api_case(self, argData, url, method, header, model=True):
        web_url = f'{HOST}' + url
        payload = argData
        if method == 'POST':
            resp = requests.post(web_url, payload, headers=header)
        if method == 'GET':
            resp = requests.get(web_url, payload)
        return resp.json()

# if __name__ == '__main__':
#     # 创建类的对象
#     login_obj = Login()
#     password = encryption("abc")
#     print(password)
#     res = login_obj.login(argData={'account': '900000194910010000', 'password': password, 'channel': 'pc'}, url='/ToPublic_API/login/userlogin')
#     # print(res)  # 返回的类型为json格式，查看返回内容的引号，双引号为json，单引号为字典
#     # pprint(res)
#     # print(get_md5('abc'))
#     print(res)
#     # print("-------------------------------------------------------------------")
#     # encryption('abc')   # kk6Nnls7k0MxhatSosR4gQ==

