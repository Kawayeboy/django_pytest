# -*- coding: utf-8 -*-
import pprint

import yaml
import xlrd
import xlwt
import os


class AnalysisFiles:
    '''
    封装一个遍历指定路径下的文件方法，os.walk 方法遍历返回指定路径下的 路径（root）、dirs（文件夹）、files（文件名）
    通过os.path 模块的 join 方法拼接路径加文件名，获取到相应的路径文件字串
    '''
    def get_file_path(self, dir):
        file_path_list = []
        if dir:
            for root, dirs, files in os.walk(dir):
                for file in files:
                    # print(os.path.join(root, file))
                    file_path_list.append(os.path.join(root, file))
        return file_path_list

    '''
    封装一个解析用例文件内容的方法,判断文件类型调用不同的解析方法
    '''
    # def analysis_files_data(self, dir='../caseData/'):
    #     file_path_list = self.get_file_path(dir)
    #     yaml_list = []
    #     excel_list = []
    #     para_dict = {}
    #     for file in file_path_list:
    #         (file_name, file_expand) = os.path.splitext(file)
    #         if file_expand == '.yaml':
    #             yaml_list.append(self.analysis_yaml(file))
    #         if file_expand == '.xls':
    #             excel_list.append(self.analysis_excel(file))
    #         if file_expand == '.xlsx':
    #             excel_list.append(self.analysis_excel(file))
    #     para_dict['yaml'] = yaml_list
    #     para_dict['excel'] = excel_list
    #     # print(para_dict)
    #     return para_dict

    def analysis_files_data(self, dir='../caseData/'):
        para_resp_list = []
        file_path_list = self.get_file_path(dir)
        for file in file_path_list:
            (file_name, file_expand) = os.path.splitext(file)
            if file_expand == '.yaml':
                self.analysis_yaml(para_resp_list, file)
            if file_expand == '.xls':
                self.analysis_excel(para_resp_list, file)
            if file_expand == '.xlsx':
                self.analysis_excel(para_resp_list, file)
        return para_resp_list

    def analysis_yaml(self, para_resp_list, fileDir):
        '''
        封装一个 yaml 用例文件的解析类
        :param fileDir:  文件路径名，建议将文件放置应用路劲下后使用相对路径名，如'../caseData/case_data_login.yaml'
        :return: 返回一个包含传入参数、预期响应、接口路径和调用方法的list，list中传入参数和预期响应是一组元组。
        '''
        # python自带的文件库open方法打开文件, 读取操作 ， 编码格式设置为 utf-8
        fo = open(fileDir, 'r', encoding='utf-8')
        # yaml 库读取解析文件数据
        dataYaml = yaml.load(fo, Loader=yaml.FullLoader)  # Loader 不能写成小写，yaml大小写敏感
        # print(dataYaml)
        # para_resp_list = []
        # del dataYaml[0]  # 删除掉list 的第一个数据，因为该数据是url 和 method 数据，不包含data 和 resp
        for dataList in dataYaml:
            if dataList != dataYaml[0]:
                para_resp_list.append((dataList["data"], dataList["resp"], dataList['detail'],
                                       dataYaml[0]['urlstr'], dataYaml[0]['method'],))
        # para_resp_list.append(dataYaml[0])    # 将路径和method 内容插入到返回的 list 中
        # print(para_resp_list)
        # del para_resp_list[-1]  # 删除掉list 的最后一组数据
        fo.close()
        return para_resp_list

    def analysis_excel(self, para_resp_list, filesDir, para_index=5, resp_index=6, scenne_desc=1, url_index=2, method_index=3):
        '''
        根据固定的测试用例模板，将除文件路径以外的参数定义为可选参数（即可变参数或默认参数，非必填），不填的时候使用默认值
        :param filesDir:  文件路径
        :param para_index: 用例文件中参数存放的列的索引，索引从0开始
        :param resp_index: 用例文件中存放预期响应的列的索引，索引从0开始
        :param url_index: 用例文件中存放接口路径的列的索引，索引从0开始
        :param method_index: 用例文件中存放请求方法的列的索引，索引从0开始
        :return: 返回一个包含接口路径、接口调用方式、传入参数和预期输出的一个数据字典，字典key有：url、method、param_resp
        '''
        # 定义参数列表和预期响应列表，接口地址列表,以及最终方法返回的字典
        fo_xls = xlrd.open_workbook(filesDir, encoding_override="utf-8")  # 打开文件
        sheet_list = fo_xls.sheets()  # 获取到表格的sheet 列表
        # print(sheet_list)
        '''
        通过 row_data = sheet.row[index] 获取到某一行的数据
        通过 row_data[index] 获取到这一行中某一列的数据，但是这时候获取到的数据都是 text：value 这样的数据
        实际我们只需要 value的值，这时候通过 row_data[index].value 即可
        '''
        for sheet_index in range(len(sheet_list)):
            sheet_work = fo_xls.sheet_by_index(sheet_index)
            sheet_row_num = sheet_work.nrows
            if sheet_row_num:
                for row_index in range(sheet_row_num):
                    if row_index == 0:
                        print("第一行为表头，不需要")
                    else:
                        row_data = sheet_work.row(row_index)
                        para_resp_list.append((row_data[para_index].value, row_data[resp_index].value,
                                               row_data[scenne_desc].value, row_data[url_index].value,
                                               row_data[method_index].value))
        return para_resp_list


# if __name__ == '__main__':
#     AnalysisFiles().analysis_files_data('../caseData/')
    # pprint.pprint(para_dict)
    # para_yaml = para_dict['yaml']
    # # pprint.pprint(para_yaml)
    # for para in para_yaml:
    #     pprint.pprint(para)
    # para_excel = para_dict['excel']
    # # pprint.pprint(para_excel)
    # for para in para_excel:
    #     pprint.pprint(para)
    # 使用相对路径读取文件数据
    # resYaml = analysis_yaml('../caseData/case_data_login.yaml')
    # resExcel = analysis_excel("../caseData/case_data_manage.xls")
    # print("yaml表内容：-------------------------------------------------------------")
    # print(resYaml)
    # print("excel表内容：-------------------------------------------------------------")
    # print(resExcel)
    # parameter = resExcel['param_resp']
    # print(parameter)
    # for para in parameter:
    #     print(para)

